#ifndef ADDSCHEDULE_H
#define ADDSCHEDULE_H

#include <QWidget>
#include <QSqlTableModel>
namespace Ui {
class addschedule;
}

class addschedule : public QWidget
{
    Q_OBJECT

public:
    explicit addschedule(QWidget *parent = 0);
    ~addschedule();

private slots:
    void on_connButton_clicked();

    void on_displayButton_clicked();

    void on_NameBox_currentIndexChanged(int index);

    void on_addButton_clicked();

    void on_update_Button_clicked();


    void on_BackButton_clicked();

private:
    Ui::addschedule *ui;
    QSqlTableModel *model;
    QSqlDatabase db;
    QString N;
    QString Na;
};

#endif // ADDSCHEDULE_H
