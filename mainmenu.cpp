#include "mainmenu.h"
#include "appointment.h"
#include "ui_mainmenu.h"
#include "payment.h"
#include "modifyform.h"
#include "registration.h"
#include "schedule.h"
#include "login.h"
#include "qpixmap.h"
#include "qdir.h"

MainMenu::MainMenu(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainMenu)
{
    ui->setupUi(this);
    QString path;
    QDir dir;
    path=dir.currentPath()+"/menu.jpg";
    QPixmap pic(path);
    ui->background->setPixmap(pic);
}

MainMenu::~MainMenu()
{
    delete ui;
}

void MainMenu::on_appointButton_clicked()
{
    Appointment *a=new Appointment();
    a->show();
}

void MainMenu::on_exitButton_clicked()
{
    this->close();
}


void MainMenu::on_paymentButton_clicked()
{
    Payment *py=new Payment();
    py->show();
}

void MainMenu::on_modifyButton_clicked()
{
    ModifyForm *mdy=new ModifyForm();
    mdy->show();
}

void MainMenu::on_RegButton_clicked()
{
    registration *reg=new registration();
    reg->show();
}

void MainMenu::on_schedleButton_clicked()
{
    Schedule *schd=new Schedule();
    schd->show();
}
