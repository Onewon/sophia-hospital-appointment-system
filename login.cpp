#include "login.h"
#include "mainmenu.h"
#include "ui_login.h"
#include <iostream>
#include <QMessageBox>
#include <QPixmap>
#include <QDir>
using namespace std;
Login::Login(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
    QString path;
    QDir dir;
    path=dir.currentPath()+"/back.jpg";
    QPixmap pic(path);
    ui->background->setPixmap(pic);
}

Login::~Login()
{
    delete ui;
}

void Login::on_loginButton_clicked()
{
    if(ui->accountline->text() ==acount &&ui->pswline->text() == psw){
    MainMenu *m=new MainMenu();
    m->show();
    this->hide();
    }
    else{
        QMessageBox::critical(this,"Error","Incorrect account or password.Please try again.");
    }
}
