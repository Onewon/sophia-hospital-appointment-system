#ifndef MAINMENU_H
#define MAINMENU_H

#include <QWidget>

namespace Ui {
class MainMenu;
}

class MainMenu : public QWidget
{
    Q_OBJECT

public:
    explicit MainMenu(QWidget *parent = 0);
    ~MainMenu();

private slots:
    void on_appointButton_clicked();

    void on_exitButton_clicked();

    void on_paymentButton_clicked();

    void on_modifyButton_clicked();

    void on_RegButton_clicked();

    void on_schedleButton_clicked();

private:
    Ui::MainMenu *ui;
};

#endif // MAINMENU_H
