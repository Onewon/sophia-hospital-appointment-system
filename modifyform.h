﻿#ifndef MODIFYFORM_H
#define MODIFYFORM_H

#include <QWidget>
#include <QSqlDatabase>
#include <QSqlTableModel>
namespace Ui {
class ModifyForm;
}

class ModifyForm : public QWidget
{
    Q_OBJECT

public:
    explicit ModifyForm(QWidget *parent = 0);
    ~ModifyForm();


private slots:
    bool on_connectButton_clicked();

    void on_displayButton_clicked();

    void on_updateButton_clicked();

    void on_deleteButton_clicked();

    void on_sortButton_clicked();

    void on_BackButton_clicked();

private:
    Ui::ModifyForm *ui;
    QSqlDatabase db;
    QSqlTableModel *model;
};

#endif // MODIFYFORM_H

