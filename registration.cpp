#include "registration.h"
#include "ui_registration.h"

#include "qmessagebox.h"
#include <iostream>
#include <QDebug>
#include <QSqlTableModel>
#include <QTableView>
#include <QSqlDatabase>
#include <QSqlError>
#include <QsqlQueryModel>
#include <QSqlQuery>
#include <QDate>


int NP=1;

registration::registration(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::registration)
{

    ui->setupUi(this);
}

registration::~registration()
{
    delete ui;
}

void registration::on_backButton_clicked()
{
    this->hide();
}

void registration::on_clearButton_clicked()
{
    ui->name->setText("");
    ui->gender->setText("");
    ui->race->setText("");
    ui->adr->setText("");
    ui->birthdate->setText("");
    ui->country->setText("");
    ui->idnumber->setText("");
    ui->phn->setText("");
}

void registration::on_pushButton_clicked()
{
    QMessageBox::information(this,"Title","connecting");
    QSqlDatabase db=QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setDatabaseName("hospitaldatabase");
    db.setUserName("root");
    db.setPassword("123456");
    if(!db.open()){
        QMessageBox::critical(0,"DataBase Error",db.lastError().text());
    }
    else{QMessageBox::information(0,"Info","Connect successfully.");}
    QSqlQuery query;
    QString text="select Max(patientinfo.Index) from patientinfo;";
    query.exec(text);
    query.next();
    QString N=query.value(0).toString();
    N.replace("NP","");
    NP=N.toInt()+1;
    QString np="NP"+QString::number(NP);
    ui->index->setText(np);
    QLocale locale = QLocale::English;
    QDate nowdate= QDate::currentDate();
    ui->regdate->setText(locale.toString(nowdate));
}

void registration::on_addButton_clicked()
{
    QSqlQuery query;
    QString t1=ui->name->text();
    QString t2=ui->gender->text();
    QString t3=ui->race->text();
    QString t4=ui->adr->text();
    QString t5=ui->birthdate->text();
    QString t6=ui->country->text();
    QString t7=ui->idnumber->text();
    QString t8=ui->phn->text();
    QString t9=ui->index->text();
    QString t10=ui->regdate->text();
    QString text=QString("insert into patientinfo values('%1','%2','%3','%4','%5','%6','%7','%8','%9','"+t10+"')")
            .arg(t1,t2,t3,t4,t5,t6,t7,t8,t9);
    NP+=1;
    QString np="NP"+QString::number(NP);
    ui->index->setText(np);
    QMessageBox::information(0,"Info","Add new patient successfully.");
    query.exec(text);
}
