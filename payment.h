#ifndef PAYMENT_H
#define PAYMENT_H

#include <QWidget>
#include <QsqlQueryModel>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QQueue>


namespace Ui {
class Payment;
}

class Payment : public QWidget
{
    Q_OBJECT
public:
    QSqlDatabase db;
    QQueue<QString> Qpayment;
    explicit Payment(QWidget *parent = 0);
    ~Payment();


private slots:
    bool on_ConnectButton_clicked();

    void on_ListButton_clicked();

    void on_search_name_Button_clicked();


    void on_search_date_Button_clicked();

    void on_backButton_clicked();

    void on_addButton_clicked();

    void on_popButton_clicked();

    void on_collectButton_clicked();

    void on_frontButton_clicked();

    void on_thebackButton_clicked();

    void on_displayButton_clicked();

    void on_tableView_clicked(const QModelIndex &index);

private:
    Ui::Payment *ui;
    QSqlQueryModel *model;
    QString list;
};

#endif // PAYMENT_H
