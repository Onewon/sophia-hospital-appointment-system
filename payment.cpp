#include "payment.h"
#include "ui_payment.h"
#include "qmessagebox.h"

#include <iostream>
#include <QDebug>
#include <QSqlTableModel>
#include <QTableView>
#include <QSqlDatabase>
#include <QSqlError>
#include <QsqlQueryModel>
#include <QSqlQuery>
using namespace std;


Payment::Payment(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Payment)
{
    ui->setupUi(this);
}

Payment::~Payment()
{
    delete ui;
}


bool Payment::on_ConnectButton_clicked()
{
    QMessageBox::information(this,"Title","connecting");

    QSqlDatabase db=QSqlDatabase::addDatabase("QMYSQL");

    db.setHostName("localhost");
    db.setDatabaseName("hospitaldatabase");
    db.setUserName("root");
    db.setPassword("123456");
    if(!db.open()){
        QMessageBox::critical(0,"DataBase Error",db.lastError().text());
        return false;
    }
    else{
        QMessageBox::information(0,"Info","Connect successfully.");
        return true;
    }
}


void Payment::on_ListButton_clicked()
{   model = new QSqlQueryModel(ui->tableView);
    QSqlQuery qry("", db);
    qry.exec("select * from payment");
    model->setQuery(qry);
    ui->tableView->setModel(model);
}

void Payment::on_search_name_Button_clicked()
{   QSqlQuery q("", db);
    QString name=ui->search_name->text();
    QSqlQueryModel *model2 = new QSqlQueryModel(ui->tableView);
    q.exec(QString("select * from payment where Name='%1'").arg(name));
    model2->setQuery(q);
    ui->tableView->setModel(model2);
}
void Payment::on_search_date_Button_clicked()
{
    QSqlQuery qry2("", db);
    QString date=QString("%1-%2-%3")
            .arg(ui->year->currentText(),ui->month->currentText(),ui->day->currentText());
    QSqlQueryModel *model3 = new QSqlQueryModel(ui->tableView);
    qry2.exec(QString("select * from payment where Date='%1'").arg(date));
    model3->setQuery(qry2);
    ui->tableView->setModel(model3);
}

void Payment::on_backButton_clicked()
{
    this->hide();
}


void Payment::on_addButton_clicked()
{    Qpayment.enqueue(list);
     cout <<"Queue: Insert :"+Qpayment.back().toStdString()<<endl;
}

void Payment::on_popButton_clicked()
{   if(Qpayment.isEmpty()){
        cout << "Queue: Queue empty"<<endl;
    }
    else{
        cout <<"Queue: Delete :"+Qpayment.front().toStdString()<<endl;
        Qpayment.dequeue();
        }
}

void Payment::on_collectButton_clicked()
{
    cout << "Queue: There are currently " << to_string(Qpayment.size()) << " payments in the queue" << endl;
}

void Payment::on_frontButton_clicked()
{   if(Qpayment.isEmpty()){
        cout << "Queue: Queue empty"<<endl;
    }
    else{
    cout << "Queue: The payment at the front of the queue is " <<Qpayment.front().toStdString() << endl;
    }
}

void Payment::on_thebackButton_clicked()
{   if(Qpayment.isEmpty()){
        cout << "Queue: Queue empty"<<endl;
    }
    else{
    cout << "Queue: The payment at the back of the queue is " <<Qpayment.back().toStdString() << endl;
    }
}

void Payment::on_displayButton_clicked()
{
    while(!Qpayment.isEmpty()){
        cout <<Qpayment.front().toStdString()<<endl;
        Qpayment.dequeue();
    }
    cout <<"Queue: The queue is empty."<<endl;
}

void Payment::on_tableView_clicked(const QModelIndex &index)
{
    int curRow =index.row();
    QString name= model->index(curRow,0,QModelIndex()).data().toString();
    QString indexx= model->index(curRow,1,QModelIndex()).data().toString();
    QString date= model->index(curRow,2,QModelIndex()).data().toString();
    QString time= model->index(curRow,3,QModelIndex()).data().toString();
    QString money= model->index(curRow,6,QModelIndex()).data().toString();
    list="['"+name+"','"+indexx+"','"+date+"','"+time+"','"+money+"']";
}
