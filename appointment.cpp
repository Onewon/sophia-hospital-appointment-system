#include "appointment.h"
#include "ui_appointment.h"

#include <iostream>
#include <stdlib.h>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QPixmap>
#include <QDir>
using namespace std;

Appointment::Appointment(QWidget *parent) :
    QWidget(parent),

    ui(new Ui::Appointment)
{
    ui->setupUi(this);
    QString path;
    QDir dir;
}

Appointment::~Appointment()
{
    delete ui;
}

void Appointment::on_backButton_clicked()
{
    this->hide();
}

void Appointment::on_displayButton_clicked()
{
    QMessageBox::information(this,"Title","connecting");
    QSqlDatabase db=QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setDatabaseName("hospitaldatabase");
    db.setUserName("root");
    db.setPassword("123456");
    if(!db.open()){
        QMessageBox::critical(0,"DataBase Error",db.lastError().text());
    }
    else{QMessageBox::information(0,"Info","Connect successfully.");}
    model = new QSqlTableModel(ui->tableView);
    model->setTable("schedule");
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->select();
    ui->tableView->setModel(model);
    setValues(0);
}
void Appointment::on_next_userButton_clicked()
{   QString list="['"+name+"','"+index+"','"+date+"','"+time+"','"+
            ui->conditionText->toPlainText()+"','"+ui->prescripText->toPlainText()+"']";
    st.push(list);
    n=n+1;
    setValues(n);
}
void Appointment::setValues(int n)
{   //QMessageBox::warning(0,"info",QString::number(n,10));
    if (n==st.maxnos())
    {
        QMessageBox::warning(0,"Error","No more available appointment.");
    }
    else
    {
        name= model->index(n,0,QModelIndex()).data().toString();
        index= model->index(n,1,QModelIndex()).data().toString();
        date= model->index(n,2,QModelIndex()).data().toString();
        time= model->index(n,3,QModelIndex()).data().toString()+"-"+
                model->index(n,4,QModelIndex()).data().toString();
        ui->nameBrowser->setText(name);
        ui->indexBrowser->setText(index);
        ui->dateBrowser->setText(date);
        ui->timeBrowser->setText(time);
        int i=n%4+1;
        QPixmap pic(QString(dir.currentPath()+"/p%1.jpg").arg(i));
        ui->Photo->setPixmap(pic);
    }
}

void Appointment::on_disButton_clicked()
{
    st.display();
}
void Appointment::on_popButton_clicked()
{
    st.pop();
}
void Appointment::on_sizeButton_clicked()
{
    cout <<"Stack: The size of stack is "+to_string(st.size())<<endl;
}
void Appointment::on_topElementButton_clicked()
{    if (st.isEmpty())
    {
        cout << "Stack: Stack is empty. Underflow condition! " << endl;
    }
    else
    {
    cout <<"Stack: The top element of stack is :"+st.topElement()<<endl;
    }}

/*void Appointment::on_comboBox_2_activated(const QString &arg1)
{
    //ui->conditionText->setText(arg1);
}

void Appointment::on_comboBox_activated(const QString &arg1)
{
    //ui->prescripText->setText(arg1);
}
bool Appointment::ismaxno(int n)
{   int maxno=st.maxnos();
    if (n==maxno){ return true;}
    else        { return false;}
}*/
