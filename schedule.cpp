#include "schedule.h"
#include "ui_schedule.h"
#include "addschedule.h"
#include <QSqlDatabase>
#include <QSqlError>
#include <QsqlQueryModel>
#include <QSqlQuery>
#include <QDate>
#include <QMessageBox>
#include <QSqlTableModel>

Schedule::Schedule(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Schedule)
{
    ui->setupUi(this);
}

Schedule::~Schedule()
{
    delete ui;
}





void Schedule::on_connButton_clicked()
{
    QMessageBox::information(this,"Title","connecting");

    QSqlDatabase db=QSqlDatabase::addDatabase("QMYSQL");

    db.setHostName("localhost");
    db.setDatabaseName("hospitaldatabase");
    db.setUserName("root");
    db.setPassword("123456");
    if(!db.open()){
        QMessageBox::critical(0,"DataBase Error",db.lastError().text());
    }
    else{QMessageBox::information(0,"Info","Connect successfully.");}

}

void Schedule::on_displayButton_clicked()
{
    model = new QSqlTableModel(ui->tableView);
    model->setTable("schedule");
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->select();
    ui->tableView->setModel(model);//TableVIew initialization
}


void Schedule::on_AddButton_clicked()
{
    addschedule *add=new addschedule();
    add->show();
}

void Schedule::on_sortButton_clicked()
{
    model->setSort(2, Qt::AscendingOrder);
    model->select();
}

void Schedule::on_backButton_clicked()
{
    this->hide();
}
