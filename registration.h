#ifndef REGISTRATION_H
#define REGISTRATION_H

#include <QWidget>

namespace Ui {
class registration;
}

class registration : public QWidget
{
    Q_OBJECT

public:
    explicit registration(QWidget *parent = 0);
    ~registration();

private slots:


    void on_backButton_clicked();

    void on_clearButton_clicked();

    void on_pushButton_clicked();

    void on_addButton_clicked();

private:
    Ui::registration *ui;


};

#endif // REGISTRATION_H
