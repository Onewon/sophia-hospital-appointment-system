#include "modifyform.h"
#include "ui_modifyform.h"
#include "modifyform.h"

#include "qmessagebox.h"

#include <QDebug>
#include <QSqlTableModel>
#include <QTableView>
#include <QSqlDatabase>
#include <QSqlError>
#include <QsqlQueryModel>
#include <QSqlQuery>
#include <QDate>


ModifyForm::ModifyForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ModifyForm)
{
    ui->setupUi(this);
}

ModifyForm::~ModifyForm()
{
    delete ui;
}


bool ModifyForm::on_connectButton_clicked()
{
    QMessageBox::information(this,"Title","connecting");

    db=QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setDatabaseName("hospitaldatabase");
    db.setUserName("root");
    db.setPassword("123456");
    if(!db.open()){
        QMessageBox::critical(0,"DataBase Error",db.lastError().text());
        return false;
    }
    else{
        QMessageBox::information(0,"Info","Connect successfully.");
        return true;
    }

}
void ModifyForm::on_displayButton_clicked()
{
    model = new QSqlTableModel(ui->tableView);

    model->setTable("PatientInfo");
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->select();
    ui->tableView->setModel(model);
}

void ModifyForm::on_updateButton_clicked()
{
    model->database().transaction();
    if (model->submitAll()) {
    model->database().commit();
    QMessageBox::information(0,"Info","Update data successfully.");
    }
    else {
    model->database().rollback();
    QMessageBox::warning(this, tr("tableModel"),
    tr("DatabaseError: %1")
    .arg(model->lastError().text()));
    }

}

void ModifyForm::on_deleteButton_clicked()
{
    int curRow = ui->tableView->currentIndex().row();
    model->removeRow(curRow);
    int ok = QMessageBox::warning(this,tr("Delete this row!"),
                                  tr("you sure delete current row?"),
    QMessageBox::Yes,QMessageBox::No);
    if(ok == QMessageBox::No)
    {
    model->revertAll();
    }
    else model->submitAll();
}
void ModifyForm::on_sortButton_clicked()
{   model->setSort(8, Qt::AscendingOrder);
    model->select();}

void ModifyForm::on_BackButton_clicked()
{
    this->hide();
}
