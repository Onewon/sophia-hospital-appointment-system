#ifndef APPOINTMENT_H
#define APPOINTMENT_H

#include <QWidget>
#include <QSqlTableModel>
#include <QSqlQuery>
#include <QMessageBox>
#include <QDir>
#include <iostream>
#include <time.h>
#include <random>

using namespace std;
namespace Ui {
class Appointment;
class stack;
}

class stack {
    QString stk[5];
    int top;

public:
    stack()
    {
        top = -1;
    }
    void push(QString x)
    {
        if (top >= 4) {
            cout << "Stack: Stack overflow"<<endl;
                    return;}
     else{
            stk[++top] = x;
        cout << "Stack: Inserted :" << x.toStdString()<<endl;}}
    void pop()
    {
        if (isEmpty())
        {
            cout << "Stack: Stack underflow"<<endl;
                 return;
        }
        QString popelement=stk[top--];
        cout << "Stack: Deleted :" <<popelement.toStdString()<<endl;
        popelement.replace('[',' ');
        popelement.replace(']',' ');

        std::default_random_engine random(time(NULL));
        std::uniform_int_distribution<int> dis(5, 50);
        int randomnos = dis(random)*10;
        QString randomnos_tostring=QString::number(randomnos,10);
        QString message="Total bill is RM"+randomnos_tostring+".\nAdd payment into database successfully.";
        QMessageBox::information(0,"Info",message);

        QSqlQuery query;
        QString text=QString("INSERT INTO payment VALUES (%1,'RM%2');").arg(popelement,randomnos_tostring);
        query.exec(text);
        query.next();
    }
    bool isEmpty()
    { if (top == -1)
            return true;
      else  return false;    }
    void display()
    {
        if (isEmpty())
        {
            cout << "Stack: Stack empty"<<endl;
            return;
        }
        for (int i = top; i >= 0; i--)
            cout << "Stack: "+stk[i].toStdString() <<" "<<endl;
    }
    int size()
    {
        return top + 1;
    }
    string topElement()
    {
        return stk[top].toStdString();
    }
    int maxnos()
    {
    QSqlQuery query;
    QString text="select Max(schedule.Index) from schedule;";
    query.exec(text);
    query.next();
    QString N=query.value(0).toString();
    N.replace("NP","");
    int nos=N.toInt();
    return nos;
    }
};
class Appointment : public QWidget
{
    Q_OBJECT

public:
    explicit Appointment(QWidget *parent = 0);
    stack st;
    ~Appointment();

private slots:
    void on_backButton_clicked();

    void on_displayButton_clicked();

    void on_next_userButton_clicked();

    void setValues(int n);

    void on_disButton_clicked();

    void on_popButton_clicked();

    void on_sizeButton_clicked();

    void on_topElementButton_clicked();

    //void on_comboBox_2_activated(const QString &arg1);

    //void on_comboBox_activated(const QString &arg1);

    //bool ismaxno(int n);

private:
    Ui::Appointment *ui;
    QSqlTableModel * model;
    QString name;
    QString index;
    QString date;
    QString time;
    QDir dir;
    int n=0;
};


#endif // APPOINTMENT_H
