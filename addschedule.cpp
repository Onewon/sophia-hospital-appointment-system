#include "addschedule.h"
#include "ui_addschedule.h"
#include <QSqlDatabase>
#include <QSqlError>
#include <QsqlQueryModel>
#include <QSqlQuery>
#include <QDate>
#include <QMessageBox>
#include <QSqlTableModel>

#include <iostream>

addschedule::addschedule(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::addschedule)
{
    ui->setupUi(this);
}

addschedule::~addschedule()
{
    delete ui;
}

void addschedule::on_connButton_clicked()
{
    QMessageBox::information(this,"Title","connecting");

    db=QSqlDatabase::addDatabase("QMYSQL");

    db.setHostName("localhost");
    db.setDatabaseName("hospitaldatabase");
    db.setUserName("root");
    db.setPassword("123456");
    if(!db.open()){
        QMessageBox::critical(0,"DataBase Error",db.lastError().text());
    }
    else{QMessageBox::information(0,"Info","Connect successfully.");}
}

void addschedule::on_displayButton_clicked()
{
    model = new QSqlTableModel(ui->tableView);
    model->setTable("schedule");
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->select();
    ui->tableView->setModel(model);//TableVIew initialization

    QSqlQueryModel *modal=new QSqlQueryModel();
    QSqlQuery query;
    QString text="SELECT name FROM schedule;";
    query.prepare(text);
    query.exec();
    modal->setQuery(query);//Querymodel initialization

    ui->NameBox->setModel(modal);//display all name

    QModelIndex index=model->index(0,1,QModelIndex());
    QString str= index.data().toString();
    ui->indexLine->setText(str);
}

void addschedule::on_NameBox_currentIndexChanged(int indexx)
{
    //int selrow=ui->NameBox->currentIndex();
    QModelIndex index=model->index(indexx,1,QModelIndex());
    QString str= index.data().toString();
    ui->indexLine->setText(str);
}


void addschedule::on_addButton_clicked()
{
    QSqlQuery query;
       query.exec("select Max(patientinfo.Index) from patientinfo;");
       if(query.first())
       {
           N=query.value(0).toString();
       }
       query.exec(QString("select name from patientinfo WHERE `Index`='%1';").arg(N));
       if(query.first())
       {
           Na=query.value(0).toString();
       }

    QString text=QString("insert into schedule values('%1','%2','2018-01-15','8:00','8:30');").arg(Na,N);
    query.prepare(text);
    query.exec();

    QMessageBox::information(this,"Title","Add successfully.");

}

void addschedule::on_update_Button_clicked()
{
        //QString str=ui->indexLine->text();
        QString date=QString("%1-%2-%3").arg(ui->year->currentText(),ui->month->currentText(),ui->day->currentText());
        QString qtime=ui->time->currentText();
        QStringList timelist = qtime.split('-');
        QString qry=QString("UPDATE schedule SET `Date`='%2',`StartTime`='%3',`EndTime`='%4' WHERE `Name`='%1';").arg(ui->NameBox->currentText(),date,timelist[0],timelist[1]);
        QSqlQuery query;
        query.prepare(qry);
        query.exec();
        QMessageBox::information(this,"Title","Update successfully.");
        //std::cout<<(qry.toStdString());
}


void addschedule::on_BackButton_clicked()
{
    this->hide();
}
