#-------------------------------------------------
#
# Project created by QtCreator 2017-12-07T11:18:45
#
#-------------------------------------------------

QT       += core gui
QT       += sql
CONFIG   += console

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HospitalSys
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        login.cpp \
    mainmenu.cpp \
    appointment.cpp \
    payment.cpp \
    modifyform.cpp \
    schedule.cpp \
    registration.cpp \
    addschedule.cpp \
    addschedule.cpp

HEADERS += \
        login.h \
    mainmenu.h \
    appointment.h \
    payment.h \
    modifyform.h \
    schedule.h \
    registration.h \
    addschedule.h \
    addschedule.h

FORMS += \
        login.ui \
    mainmenu.ui \
    appointment.ui \
    payment.ui \
    modifyform.ui \
    schedule.ui \
    registration.ui \
    addschedule.ui \
    addschedule.ui \
    addschedule.ui
