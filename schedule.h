#ifndef SCHEDULE_H
#define SCHEDULE_H

#include <QWidget>
#include <QSqlTableModel>
namespace Ui {
class Schedule;
}

class Schedule : public QWidget
{
    Q_OBJECT

public:
    explicit Schedule(QWidget *parent = 0);
    ~Schedule();

private slots:
    void on_connButton_clicked();

    void on_displayButton_clicked();

    void on_AddButton_clicked();

    void on_sortButton_clicked();

    void on_backButton_clicked();

private:
    Ui::Schedule *ui;
    QSqlTableModel *model;
};

#endif // SCHEDULE_H
